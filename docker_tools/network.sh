#!/bin/bash

function network_init
{
    if [ $# -ne 1 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} network_name"
        return 1
    fi
    local network_name=$1; shift;

    docker network ls | grep overlay | grep ${network_name} >/dev/null
    if [ $? -eq 1 ]
    then
        docker network create \
                   --driver overlay \
                   --opt encrypted \
                   ${network_name}
    fi
}
