#!/bin/bash

function swarm_init_manager
{
    if [ $# -ne 1 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} ip"
        return 1
    fi
    local ip=$1; shift;

    ssh root@${ip} -- "docker swarm init --advertise-addr ${ip}" >/dev/null
}

function swarm_add_node
{
    if [ $# -ne 3 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} node node_type manager_ip"
        return 1
    fi
    local node=$1; shift;
    local node_type=$1; shift;
    local manager_ip=$1; shift;

    local token=`docker swarm join-token -q ${node_type}`

    ssh root@${node} -- docker swarm join --token ${token} ${manager_ip}:2377
}

function swarm_list_nodes
{
    docker node ls
}

function swarm_init
{
    if [ $# -lt 2 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} main_manager_ip main_manager_public_ip node1 type1 node2 type2 etc."
        return 1
    fi
    
    # Create a manager
    local manager_ip=$1; shift;
    swarm_init_manager ${manager_ip}

    local manager_public_ip=$1; shift;

    # Create workers
    while [ $# -gt 0 ]
    do
        swarm_add_node $1 $2 ${manager_public_ip};
        shift 2;
    done
}
