#!/bin/bash

function registry_start
{
    if [ $# -ne 1 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} node"
        return 1
    fi

    local node=$1; shift;

    local registry_dir="/mnt/registry";

    docker node update --label-add registry=true ${node}

    ssh root@${node} -- mkdir -p ${registry_dir}
    docker service create \
        --name registry \
        --constraint 'node.labels.registry==true' \
        --mount type=bind,src=${registry_dir},dst=/var/lib/registry \
        -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 \
        --publish published=5000,target=5000 \
        --replicas 1 \
        registry:2
}

#function registry_tag_and_push
#{
#    if [ $# -ne 3 ]
#    then
#        >&2 echo "Usage: ${FUNCNAME[0]} registry_node image tag"
#        return 1
#    fi
#    local registry_node=$1; shift;
#    local image=$1; shift;
#    local tag=$1; shift;
#    
#    docker tag ${image} ${registry_node}:5000/${image}:${tag}
#    docker push ${registry_node}:5000/${image}
#}
