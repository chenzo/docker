#!/bin/bash

for container in `docker ps -a | tail -n +2 | awk '{print $1}'`;
do
    docker stop $container;
    docker rm $container;
done

for v in `docker volume ls -q`;
do
    docker volume rm $v;
done
