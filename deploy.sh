#!/bin/bash

# conf
. ./conf/swarm.conf

# Sources
. ./docker_tools/network.sh
. ./docker_tools/registry.sh
. ./docker_tools/swarm.sh

# Install docker packages

# Start docker swarm
swarm_init ${manager_ip} ${manager_public_ip} ${nodes}
swarm_list_nodes

# Set up network
network_init ${network_name}

# Set up registry
registry_start ${registry_node}

# Services


## Start visualizer
#services/visualizer.sh
