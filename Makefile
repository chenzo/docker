SRC = \
	  compose/seafile/docker-compose.yml \
	  containers/backup/Dockerfile \
	  containers/letsencrypt/Dockerfile \
	  containers/mariadb/Dockerfile \
	  containers/nginx_http/Dockerfile \
	  containers/nginx_seafile/Dockerfile \
	  containers/seafile/Dockerfile

all: start

dhparams: conf/dhparams.pem
	openssl dhparam -out conf/dhparams.pem 2048

build: stop $(SRC)
	./copy_env.sh
	cp conf/dhparams.pem containers/nginx_seafile/files/dhparams.pem
	docker-compose -f compose/seafile/docker-compose.yml build

restart: stop start

start:
	docker-compose -f compose/seafile/docker-compose.yml up -d

stop:
	docker-compose -f compose/seafile/docker-compose.yml stop

prune:
	docker container prune
	docker image prune

clean:
	./clean.sh

mrproper:
	./mrproper.sh
