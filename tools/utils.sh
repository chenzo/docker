#!/bin/bash

function get_ip
{
    if [ $# -ne 1 ]
    then
        >&2 echo "Usage: ${FUNCNAME[0]} device_name"
        return 1
    fi
    local device_name="$1"; shift;

    ip a show dev ${device_name} scope global \
        | grep "${device_name}" \
        | grep inet \
        | awk '{print $2}' \
        | awk -F '/' '{print $1}'
}
