#!/bin/bash

# Configure seafile at first start
/local/configure_seafile.sh

# Start the service
sleep 5; # waits for mariadb to start
export SEAFILE_FASTCGI_HOST=`hostname -i`;
/local/seafile/seafile-server-latest/seafile.sh start;
/local/seafile/seafile-server-latest/seahub.sh start-fastcgi;

sleep 5;
mkdir -p /local/volumes/seafile/conf;
cp -r /local/seafile/conf/* /local/volumes/seafile/conf/.;
mkdir -p /local/volumes/seafile/ccnet;
cp -r /local/seafile/ccnet/* /local/volumes/seafile/ccnet/.;
mkdir -p /local/volumes/seafile/logs;
mkdir -p /local/volumes/seafile/pids;
mkdir -p /local/volumes/seafile/seahub-data;
cp -r /local/seafile/seahub-data/* /local/volumes/seafile/seahub-data/.;

tail -f /local/seafile/logs/seahub.log;
