#!/bin/bash 

server_name=`hostname -s`;
server_ip=`hostname -i`;
seafile_port="8082";
seahub_port="8000";
seafile_volumes="/local/volumes/";
seafile_data_path="${seafile_volumes}/seafile/seafile-data";
mysql_host="mariadb";
mysql_port="3306";
mysql_database_ccnet="ccnet-db";
mysql_database_seafile="seafile-db";
mysql_database_seahub="seahub-db";

echo "#!/bin/bash

# If this container is not configured
if [ ! -f /local/seafile/conf/ccnet.conf ]
then
    # If there is no backup to load, then create a new configuration
    if [ ! -f /local/volumes/seafile/conf/ccnet.conf ]
    then
        echo \"Configure seafile from scratch\";

        mkdir -p /local/seafile/conf;
        mv /local/admin.txt /local/seafile/conf/admin.txt;

        mkdir ${seafile_volumes}/seafile;
        cd /local/seafile/seafile-server-${SEAFILE_VERSION} && \\
        ./setup-seafile-mysql.sh auto \\
            -n ${server_name} \\
            -i ${server_ip} \\
            -p ${seafile_port} \\
            -d ${seafile_data_path} \\
            -e 1 \\
            -o ${mysql_host} \\
            -t ${mysql_port} \\
            -u ${MARIADB_SEAFILE_USER} \\
            -w ${MARIADB_SEAFILE_PASSWORD} \\
            -c ${mysql_database_ccnet} \\
            -s ${mysql_database_seafile} \\
            -b ${mysql_database_seahub};
        echo \"FILE_SERVER_ROOT = 'https://${SERVICE_DOMAIN_NAME}/seafhttp'\" \\
            >> /local/seafile/conf/seahub_settings.py;
    # Else load the last backup.
    else
        echo \"Configure seafile from backup files\";

        cp -r /local/volumes/seafile/conf /local/seafile/conf;
        cp -r /local/volumes/seafile/ccnet /local/seafile/ccnet;
        cp -r /local/volumes/seafile/logs /local/seafile/logs;
        cp -r /local/volumes/seafile/pids /local/seafile/pids;
        cp -r /local/volumes/seafile/seahub-data /local/seafile/seahub-data;
        ln -s \\
            /local/seafile/seafile-server-${SEAFILE_VERSION} \\
            /local/seafile/seafile-server-latest;

         mysql \\
            -h mariadb \\
            -u ${MARIADB_SEAFILE_USER} \\
            --password=${MARIADB_SEAFILE_PASSWORD} \\
            ccnet-db \\
            -e \"select count(*) from EmailUser;\";

        res=\$?;
        # inject database backup
        if [ \$res -ne 0 ]
        then
            echo \"Configure seafile from backup databases\";
            mysql \\
                -h mariadb \\
                -u ${MARIADB_SEAFILE_USER} \\
                --password=${MARIADB_SEAFILE_PASSWORD} \\
                < /local/volumes/backup/db/seafile.sql;
        fi

        # inject seafile backup ?
    fi
fi"
