#!/bin/sh

echo "#!/bin/sh

d=\`date +%Y%m%dT%H%M%S\`;
src_dir=\"/local/volumes/seafile\";
dump_dir=\"/local/volumes/backup/seafile\";
dump_filename=\"\${dump_dir}/seafile_\${d}.tar.xz\";
dump_link=\"\${dump_dir}/seafile.tar.xz\";

mkdir -p \${dump_dir};

tar cJvf \${dump_filename} \${src_dir};

rm \${dump_link};
ln -s \${dump_filename} \${dump_link};

find \${dump_dir} -mtime +7 -exec rm {} \\;

echo \"Dump saved: \${dump_filename}\"
";
