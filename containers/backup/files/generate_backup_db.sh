#!/bin/sh

echo "#!/bin/sh

d=\`date +%Y%m%dT%H%M%S\`;
dump_dir=\"/local/volumes/backup/db\";
dump_filename=\"\${dump_dir}/seafile_\${d}.sql\";
dump_link=\"\${dump_dir}/seafile.sql\";

mkdir -p \${dump_dir};

mysqldump \\
    -h mariadb \\
    -u ${MARIADB_SEAFILE_USER} \\
    --password=${MARIADB_SEAFILE_PASSWORD} \\
    --databases seafile-db seahub-db ccnet-db > \${dump_filename};

rm \${dump_link};
ln -s \${dump_filename} \${dump_link};

find \${dump_dir} -mtime +7 -exec rm {} \\;

echo \"Dump saved: \${dump_filename}\"
";
