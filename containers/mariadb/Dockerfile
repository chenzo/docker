FROM armhf/alpine:latest

MAINTAINER Vincent Lucas <vincent.lucas@gmail.com>

RUN apk add --update --no-cache mariadb mariadb-client

RUN mkdir -p /run/mysqld && \
    chown -R mysql:mysql /run/mysqld

RUN mkdir /local
COPY files/auth.env /local/auth.env
RUN chown -R mysql:mysql /local

USER mysql

RUN . /local/auth.env && \
    mysql_install_db --user=mysql --skip-auth-anonymous-user && \
    /usr/share/mysql/mysql.server start && \
    /usr/bin/mysqladmin -u ${MARIADB_USER} password "${MARIADB_PASSWORD}" && \
    mysql -h localhost -u ${MARIADB_USER} --password=${MARIADB_PASSWORD} \
        -e "drop database test;" && \
    /usr/share/mysql/mysql.server stop

RUN . /local/auth.env && \
    /usr/share/mysql/mysql.server start && \
    mysql -h localhost -u ${MARIADB_USER} --password=${MARIADB_PASSWORD} \
        -e 'create database `ccnet-db` character set = "utf8"; \
            create database `seafile-db` character set = "utf8"; \
            create database `seahub-db` character set = "utf8"; \
        ' && \
    mysql -h localhost -u ${MARIADB_USER} --password=${MARIADB_PASSWORD} \
        -e "create user '${MARIADB_SEAFILE_USER}'@'%' \
                identified by '${MARIADB_SEAFILE_PASSWORD}'; \
        " && \
    mysql -h localhost -u ${MARIADB_USER} --password=${MARIADB_PASSWORD} \
        -e "GRANT ALL PRIVILEGES ON \`ccnet-db\`.* \
                to \`${MARIADB_SEAFILE_USER}\`@'%'; \
            GRANT ALL PRIVILEGES ON \`seafile-db\`.* \
                to \`${MARIADB_SEAFILE_USER}\`@'%'; \
            GRANT ALL PRIVILEGES ON \`seahub-db\`.* \
                to \`${MARIADB_SEAFILE_USER}\`@'%'; \
        " && \
    /usr/share/mysql/mysql.server stop

RUN rm /local/auth.env

# root on localhost only
# reload priviledge tables
#    /usr/bin/mysql_secure_installation

#'/usr/bin/mysqladmin' -u root -h eb837c2b9864 password 'new-password'


EXPOSE 3306

CMD ["mysqld"]
