server {
    listen 80;
    server_name __SERVICE_DOMAIN_NAME__;

    location '/.well-known/acme-challenge' {
        default_type "text/plain";
        root /local/acme/challenge;
    }

    location '/' {
        # rewrite ^ https://$http_host$request_uri? permanent;
        return 301 https://$http_host$request_uri;

    }
}
