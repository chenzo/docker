#!/bin/sh

challenge_dir="/local/acme/challenge/.well-known/acme-challenge";
public_certificates_dir="/local/acme/public_certificates";

mkdir -p ${challenge_dir} ${public_certificates_dir};

# rm /local/acme/*.pem;

acme-client -vv -nN \
    -a https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf \
    -C ${challenge_dir} \
    -c ${public_certificates_dir} \
    -f /local/acme/account_key.pem \
    -k /local/acme/domain_key.pem \
    __SERVICE_DOMAIN_NAME__

cat ${public_certificates_dir}/cert.pem \
    ${public_certificates_dir}/chain.pem \
    > ${public_certificates_dir}/nginx.pem
