#!/bin/bash

for container in `docker ps -a | tail -n +2 | awk '{print $1}'`;
do
    docker stop $container;
    docker rm $container;
done

for image in `docker images | tail -n +2 | awk '{print $3}'`;
do
    docker rmi $image;
done
